import {
    ExecutionContext,
    HttpException,
    HttpStatus,
    Injectable,
    UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ErrorMessage } from 'enums';
import { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';

@Injectable()
export class AccessToken extends AuthGuard('access-token') {
    canActivate(context: ExecutionContext) {
        return super.canActivate(context);
    }

    handleRequest(err, user, info) {
        if (info instanceof TokenExpiredError) {
            throw new HttpException(ErrorMessage.TokenExpired, HttpStatus.UNAUTHORIZED);
        }

        if (info instanceof JsonWebTokenError) {
            throw new HttpException(ErrorMessage.TokenInvalid, HttpStatus.UNAUTHORIZED);
        }

        if (String(info)?.includes('No auth token')) {
            throw new HttpException(ErrorMessage.TokenNotExist, HttpStatus.UNAUTHORIZED);
        }

        if (err || !user) {
            throw err || new UnauthorizedException();
        }

        return user;
    }
}
