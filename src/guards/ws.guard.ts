import { CanActivate, ExecutionContext, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IAppConfig } from 'configs';
import { CommonStatus } from 'enums';
import { verify } from 'jsonwebtoken';

@Injectable()
export class WsGuard implements CanActivate {
    constructor(private readonly configService: ConfigService) {}

    private readonly logger = new Logger('WsGuard');

    canActivate(context: ExecutionContext) {
        try {
            const {
                auth: { appAccessTokenSecretKey },
            } = this.configService.get<IAppConfig>('commons');
            const token = context.switchToWs().getClient().handshake.auth.token;
            const decode = verify(token, appAccessTokenSecretKey) as any;
            if (decode?.isDeleted === CommonStatus.InActive) {
                context.switchToWs().getClient().disconnect(true);
                return false;
            }
            return true;
        } catch (error) {
            this.logger.error(error);
            context.switchToWs().getClient().disconnect(true);
            return false;
        }
    }
}
