import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class Public extends AuthGuard('public') {
    handleRequest(err, user, info): any {
        return true;
    }
}
