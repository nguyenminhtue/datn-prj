require('dotenv').config();
import { DataSource, DataSourceOptions } from 'typeorm';

const AppDataSource = new DataSource({
    type: 'mysql',
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    synchronize: true,
    timezone: 'Z',
    logging: false,
    entities: [__dirname + '/../entities/*.entity{.js, .ts}'],
    migrations: [__dirname + '/../database/migrations/*'],
} as DataSourceOptions);

export default AppDataSource;
