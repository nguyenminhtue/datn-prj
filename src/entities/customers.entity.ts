import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'customers' })
export class Customers {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'email', type: 'varchar' })
    email: string;

    @Column({ name: 'name', type: 'varchar', length: 50 })
    name: string;

    @Column({ name: 'phone_number', type: 'varchar', length: 20 })
    phoneNumber: string;

    @Column({ name: 'id_card', type: 'varchar', length: 20 })
    idCard: string;
}
