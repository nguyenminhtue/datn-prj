import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'events' })
export class Events {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'customer_id', type: 'int' })
    customerId: number;

    // 2 loai: tiec cuoi va hoi nghi
    @Column({ name: 'event_type', type: 'tinyint' })
    eventType: string;

    @Column({ name: 'booking_time', type: 'datetime' })
    bookingTime: string | Date;

    @Column({ name: 'end_time', type: 'datetime' })
    endTime: string | Date;

}
