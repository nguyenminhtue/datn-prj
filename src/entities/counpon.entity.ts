import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'coupons' })
export class Coupons {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'code', type: 'varchar', length: 20 })
    code: string;

    @Column({ name: 'coupon_persent', type: 'int', default: 0 })
    couponPersent: number;

    @Column({ name: 'is_deleted', type: 'tinyint', default: 0 })
    isDeleted: number;

    @Column({ name: 'expried_at', type: 'datetime' })
    expriedAt: string | Date;
}
