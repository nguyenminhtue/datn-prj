import * as moment from 'moment';
import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';

@Entity({ name: 'bookings' })
export class Bookings {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'checkin_date', type: 'datetime' })
    checkinDate: string | Date;

    @Column({ name: 'checkout_date', type: 'datetime' })
    checkoutDate: string | Date;

    //1: chua dat coc, 2: da coc, 3: da thanh toan
    @Column({ name: 'status', type: 'tinyint' })
    status: number;

    //dat coc
    @Column({ name: 'deposit', type: 'int', nullable: true, default: 0 })
    deposit: number;

    @Column({ name: 'totalRate', type: 'int', nullable: true, default: 0 })
    totalRate: number;

    @Column({ name: 'is_deleted', type: 'tinyint', default: 0 })
    isDeleted: number;

    @Column({ name: 'customer_id', type: 'int' })
    customerId: number;

    @Column({ name: 'coupon_id', type: 'int', nullable: true })
    couponId: number;

    @CreateDateColumn({ name: 'created_at', type: 'datetime' })
    createdAt: string | Date;

    @Column({ name: 'updated_at', type: 'datetime', default: moment(new Date()).format('YYYY-MM-DD HH:mm:ss') })
    updatedAt: string | Date;
}
