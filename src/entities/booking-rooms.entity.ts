import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'booking_rooms' })
export class BookingRooms {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'booking_id', type: 'int' })
    bookingId: number;

    @Column({ name: 'room_id', type: 'int' })
    roomId: number;
}
