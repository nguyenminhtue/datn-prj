import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'comments' })
export class Comments {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'room_id', type: 'int' })
    roomId: number;

    @Column({ name: 'rate', type: 'int', default: 5 })
    rate: number;

    @Column({ name: 'name', type: 'varchar', nullable: true })
    name: string;

    @Column({ name: 'email', type: 'varchar', nullable: true })
    email: string;

    @Column({ name: 'description', type: 'varchar', nullable: true })
    description: string;
}
