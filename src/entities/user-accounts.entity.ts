import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'user_accounts' })
export class UserAccounts {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'email', type: 'varchar', length: 255 })
    email: string;

    @Column({ name: 'password', type: 'varchar', length: 255 })
    password: string;

    @Column({ name: 'is_deleted', type: 'int', nullable: false, default: 0 })
    isDeleted: number;

    @Column({ name: 'name', type: 'varchar', length: 50, nullable: true })
    name: string;

    @Column({ name: 'gender', type: 'tinyint', nullable: true, default: 0 })
    gender: number;

    @Column({ name: 'phone_number', type: 'varchar', nullable: true })
    phoneNumber: string;

    @Column({ name: 'refresh_token', type: 'text', nullable: true })
    refreshToken: string | null;

    @Column({ name: 'token', type: 'text', nullable: true })
    token: string | null;

    @Column({ name: 'time_update_token', type: 'datetime', nullable: true })
    timeUpdateToken: string | Date;

    @Column({ name: 'description', type: 'varchar', length: 500, nullable: true })
    description: string | null;

    @Column({ name: 'banner', type: 'varchar', nullable: true })
    banner: string | null;

    @Column({ name: 'thumbnail', type: 'varchar', nullable: true })
    thumbnail: string | null;

    @Column({ name: 'role_id', type: 'int', default: 2 })
    roleId: number;

    @Column({ name: 'date_of_birth', type: 'datetime', nullable: true })
    dateOfBirth: string | Date;
}
