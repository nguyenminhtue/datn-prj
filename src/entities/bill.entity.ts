import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'bill' })
export class Bill {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'booking_id', type: 'int' })
    bookingId: number;

    @Column({ name: 'event_id', type: 'int', nullable: true })
    eventId: number;

    //tien phong
    @Column({ name: 'room_charge', type: 'int' })
    price: number;

    @Column({ name: 'description', type: 'varchar', nullable: true })
    description: string;

    @CreateDateColumn({ name: 'payment_date', type: 'datetime' })
    paymentDate: string | Date;
}
