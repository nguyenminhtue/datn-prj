import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'rooms' })
export class Rooms {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'name', type: 'varchar', length: 50 })
    name: string;

    @Column({ name: 'price', type: 'int' })
    price: number;

    @Column({ name: 'type', type: 'int', default: 1 })
    type: number;

    @Column({ name: 'thumbnail', type: 'varchar', nullable: true })
    thumbnail: string | null;

    @Column({ name: 'description', type: 'varchar', nullable: true })
    description: string;

    @Column({ name: 'is_booked', type: 'tinyint', default: 0 })
    isBooked: number;
}
