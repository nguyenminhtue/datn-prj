import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';

@Entity({ name: 'cleans' })
export class Cleans {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'user_id', type: 'int' })
    userId: number;

    @Column({ name: 'room_id', type: 'int' })
    roomId: number;

    @Column({ name: 'is_deleted', type: 'tinyint' })
    isDeleted: number;

    @Column({ name: 'status', type: 'tinyint' })
    status: number;

    @Column({ name: 'finish_date', type: 'datetime', nullable: true })
    finishDate: string | Date;
}
