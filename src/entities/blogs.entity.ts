import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'blogs' })
export class Blogs {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'title', type: 'varchar', length: 500 })
    title: string;

    @Column({ name: 'content', type: 'varchar', length: 2000 })
    content: string;

    @Column({ name: 'user_id', type: 'int' })
    userId: number;

    @Column({ name: 'thumbnail', type: 'varchar', length: 255 })
    thumbnail: string;

    @Column({ name: 'is_deleted', type: 'tinyint', default: 0 })
    isDeleted: number;

    @CreateDateColumn({ name: 'created_at', type: 'datetime' })
    createdAt: string | Date;

    @UpdateDateColumn({ name: 'updated_at', type: 'datetime' })
    updatedAt: string | Date;
}
