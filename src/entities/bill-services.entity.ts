import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'bill-services' })
export class BillServices {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'bill_id', type: 'int' })
    billId: number;

    @Column({ name: 'service_id', type: 'int', nullable: true })
    serviceId: number;

    @Column({ name: 'count', type: 'int' })
    count: number;
}
