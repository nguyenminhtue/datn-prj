import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';
import configs, { IAppConfig } from 'configs';
import { SocketsModule } from 'http/sockets/sockets.module';
import { DataSource } from 'typeorm';
import { AuthModule } from './http/app/auth/auth.module';
import { MemberModule } from './http/app/member/member.module';
import { SendgridModule } from './sendgrid/sendgrid.module';
import { BillServiceModule } from './http/app/bill-service/bill-service.module';
import { BillModule } from './http/app/bill/bill.module';
import { BlogsModule } from './http/app/blogs/blogs.module';
import { BookingsModule } from './http/app/bookings/bookings.module';
import { RoomsModule } from './http/app/rooms/rooms.module';
import { ServicesModule } from './http/app/services/services.module';
import { CleansModule } from './http/app/cleans/cleans.module';
import { CustomersModule } from './http/app/customers/customers.module';
import { CommentsModule } from './http/app/comments/comments.module';
import { CounponModule } from './http/app/counpon/counpon.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            load: [configs],
            isGlobal: true,
        }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => {
                const { database } = configService.get<IAppConfig>('commons');
                return database;
            },
            dataSourceFactory: async (options) => {
                const dataSource = await new DataSource(options).initialize();
                return dataSource;
            },
        }),
        ThrottlerModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => {
                const { throttler } = configService.get<IAppConfig>('commons');
                return throttler;
            },
        }),
        AuthModule,
        MemberModule,
        SocketsModule,
        SendgridModule,
        BillServiceModule,
        BillModule,
        BlogsModule,
        BookingsModule,
        RoomsModule,
        ServicesModule,
        CleansModule,
        CustomersModule,
        CounponModule,
        CommentsModule
    ],
    controllers: [],
    providers: [
        {
            provide: APP_GUARD,
            useClass: ThrottlerGuard,
        },
    ],
})
export class AppModule { }
