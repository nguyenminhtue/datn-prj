import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Response } from 'express';
import { isObject, omit } from 'lodash';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const status = exception.getStatus();
        const res = exception.getResponse();

        let errors;
        if (isObject(res)) {
            if (res['error']) {
                errors = {
                    ...omit(res, ['error']),
                    errorMessage: res['error'],
                };
            } else {
                errors = { ...res };
            }
        } else {
            errors = {
                errorMessage: res,
            };
        }

        response.status(status).json({
            success: false,
            statusCode: status,
            ...errors,
        });
    }
}
