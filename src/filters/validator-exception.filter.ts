import type { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
import { Catch, UnprocessableEntityException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import type { ValidationError } from 'class-validator';
import type { Response } from 'express';
import { isEmpty, snakeCase } from 'lodash';

@Catch(UnprocessableEntityException)
export class ValidatorExceptionFilter implements ExceptionFilter<UnprocessableEntityException> {
    catch(exception: UnprocessableEntityException, host: ArgumentsHost): void {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const statusCode = exception.getStatus();
        const error = exception.getResponse() as {
            statusCode: number;
            message: ValidationError[];
            error: string;
        };

        const validationErrors = error.message;
        this.validationFilter(validationErrors);

        response.status(statusCode).json({
            success: false,
            statusCode,
            errorMessage: error?.error,
            rawMessage: error.message,
        });
    }

    private validationFilter(validationErrors: ValidationError[]): void {
        for (const validationError of validationErrors) {
            const children = validationError.children;
            if (children && !isEmpty(children)) {
                this.validationFilter(children);

                return;
            }

            const constraints = validationError.constraints;
            if (!constraints) {
                return;
            }

            for (const [constraintKey, constraint] of Object.entries(constraints)) {
                if (!constraint) {
                    constraints[constraintKey] = `error.fields.${snakeCase(constraintKey)}`;
                }
            }
            delete validationError.children;
            delete validationError.target;
            delete validationError.value;
        }
    }
}
