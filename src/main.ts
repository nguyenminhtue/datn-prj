import { HttpStatus, Logger, UnprocessableEntityException, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import { IAppConfig } from 'configs';
import * as express from 'express';
import * as session from 'express-session';
import { HttpExceptionFilter, ValidatorExceptionFilter } from 'filters';
import helmet from 'helmet';
import { ResponseInterceptor } from 'interceptors';
import { loggerMiddleware } from 'middleware';
import { join } from 'path';
import { AppModule } from './app.module';

const logger = new Logger('Main');

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter(), {
        cors: true,
        logger: ['debug', 'warn'],
    });

    const configService = app.get(ConfigService);
    const {
        port,
        environment,
        auth: { nestSession },
    } = configService.get<IAppConfig>('commons');

    app.use(helmet({
        crossOriginResourcePolicy: false,
    }))
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(
        session({
            secret: nestSession,
            resave: false,
            saveUninitialized: false,
        })
    );
    app.use(loggerMiddleware);
    app.useStaticAssets(join(__dirname, '../public'));


    app.useGlobalFilters(new HttpExceptionFilter(), new ValidatorExceptionFilter());
    app.useGlobalInterceptors(new ResponseInterceptor());
    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
            transform: true,
            dismissDefaultMessages: true,
            exceptionFactory: (errors) => new UnprocessableEntityException(errors),
        })
    );
    await app.listen(port);
    logger.log(`Nest running on port ${port}. Environment ${environment}`);
}
bootstrap();
