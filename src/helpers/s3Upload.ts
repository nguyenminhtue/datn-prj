import * as aws from 'aws-sdk';

export async function pushImageToS3(image: Express.Multer.File, fileName: string) {
    const s3 = new aws.S3({
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        accessKeyId: process.env.AWS_SECRET_ACCESS_ID,
        region: process.env.AWS_REGION,
    });

    await s3
        .putObject({
            ACL: 'public-read',
            Body: image.buffer,
            Bucket: process.env.AWS_BUCKET,
            ContentType: image.mimetype,
            Key: fileName,
        })
        .promise();
}
