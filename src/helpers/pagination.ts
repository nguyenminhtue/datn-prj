export function handleReqPagination(params) {
    params.current = Number(params.current) || 1;
    params.take = Number(params.take) || 10;
    params.skip = (params.current - 1) * params.take;
    return params;
}

export function handleResPagination(data: any, totalItems: number, params) {
    return {
        data,
        current: params.current,
        totalItems,
        totalPages: Math.ceil(totalItems / params.take),
        pagination: true,
        hasMore: data ? (data.length < params.take ? false : true) : false,
    };
}
