import { random } from 'lodash';

export function randomOTP(size: number) {
    let result = '';
    for (let i = 0; i < size; i++) {
        result += random(0, 9);
    }
    return result;
}
