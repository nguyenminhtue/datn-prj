export enum ErrorMessage {
    TokenInvalid = 'Token invalid',
    TokenExpired = 'アクセストークンの有効期限が切れました',
    TokenNotExist = 'Token not exist',
    MemberBlocked = 'Member blocked',
    EmailAlreadyExist = 'Email already exist',
    VerifyCodeInvalid = 'Verify code invalid',
    MaximumRetryVerifyCode = 'Maximum retry verify code',
    Unauthorized = 'Unauthorized',
    MemberNotExist = 'Member not exist',
    ModelNotExist = 'Model is not exist',
    ModelDeleted = 'Model has been deleted',
    NotAccess = 'You do not have access',
    EmailOrPasswordIsInvalid = 'Email or password is invalid',
    InternalServerError = 'Internal Server Error',
    DeleteMessage = 'Delete false',
    DislikeModel = 'Dislike false',
    LikeModel = 'Like false',
    CreateComment = 'Create comment false',
    EditComment = 'Edit comment false',
    DeleteComment = 'Delete comment false',
    OldPasswordIsInvalid = 'Old password is invalid',
    ChangePassword = 'Change password false',
    PassWordNotCompare = 'Password not match',
    UpdateMessage = 'Update false',
    UploadMessage = 'Upload false',
    BadRequest = 'Bad request',
    AccountLinked = 'Account linked',
    UserNameExist = 'Username already exist',
    EmailNotExist = 'メールアドレスが登録されていません。',
}

export enum SuccessMessage {
    ChangeStatus = 'update status success',
    DeleteMessage = 'Delete successed',
    DislikeModel = 'Dislike successed',
    LikeModel = 'Like successed',
    CreateComment = 'Create comment successed',
    EditComment = 'Edit comment successed',
    DeleteComment = 'Delete comment successed',
    ChangePasswordMessage = 'Change password successed',
    UpdateMessage = 'Update successed',
    UploadMessage = 'Upload successed',
    CreateAccount = 'create account successed',
    ActiveAccount = 'active account successed',
    TokenValid = 'Token is valid',
    LinkedFacebook = 'Link facebook successed',
    LinkedTwitter = 'Link twitter successed',
    UserNameValid = 'User name is valid',
}

export enum CommonStatus {
    InActive = 0,
    Active = 1,
}

export enum VerifyCodeType {
    Register = 1,
}

export enum Environment {
    Development = 'development',
    Staging = 'staging',
    Production = 'production',
}

export enum MemberType {
    Normal = 1,
    Facebook = 2,
    Twitter = 3,
}

export enum DataTypes {
    number = 'number',
    string = 'string',
}

export enum DataTypeSort {
    DESC = 'DESC',
    ASC = 'ASC',
}

export enum StatusModel {
    All = 1, // no login required
    Public = 2, // must login
    GroupView = 3, // group view
}

export enum StatusUploadModel {
    NoConvert = 0,
    Converting = 1,
    Converted = 2,
}

export enum ContainerAzue {
    Original = 'original-models',
    Glb = 'glb-models',
    Usdz = 'usdz-models',
    Accounts = 'accounts',
}

export enum StatusDeleteModel {
    Deleted = 1,
    NoDelete = 0,
}

export enum StatusGroupView {
    NotActive = 0,
    Actived = 1,
}

export enum StatusConvertModel {
    NoConvert = 1,
    Converted = 2,
}

export enum StatusDeleteAccount {
    Deleted = 1,
    NoDelete = 0,
}

export enum MessageSendMail {
    SubjectForGotPass = 'Your SendGrid password reset request',
    SubjectRegister = 'Your SendGrid activate your account',
    TextForgotPass = 'Click link to reset password',
    TextRegister = 'Click the link to activate your account',
}

export enum TypeImageUser {
    Thumbnail = 1,
    Banner = 2,
}

export enum TypeSendMail {
    ActiveAccount = 'active-account',
    ResetPassword = 'reset-password',
}

export enum Roles {
    Admin = 1,
    Staff = 2,
}

export enum TemplateSendGrid {
    ActiveAccount = 'd-1ef1e75b281c4e8abfed48a15374a78f',
    ResetPassword = 'd-8d203d8da7214ad582b174f646aa28e3',
    ShareMail = 'd-feda050fa69340618c7c2984c0ad2b71'
}

export enum Role {
    Admin = 1,
    User = 2
}