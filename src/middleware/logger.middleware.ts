import { Logger } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

const logger = new Logger('HTTP');

export function loggerMiddleware(req: Request, res: Response, next: NextFunction) {
    logger.debug(
        `Method: ${req.method} | OriginalUrl: ${req.originalUrl} | Body: ${JSON.stringify(
            req.body || {}
        )}`
    );
    next();
}
