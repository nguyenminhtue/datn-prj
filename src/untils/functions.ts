import { extname, join } from 'path';
import { existsSync, mkdirSync } from 'fs';
import { diskStorage } from 'multer';
import { HttpException, HttpStatus } from '@nestjs/common';
import * as moment from 'moment';

// Multer upload options
export const multerOptions = {
    // Enable file size limits
    limits: {
        fileSize: 10000000000,
    },
    // Check the mimetypes to allow for upload
    fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) {
            // Allow storage of file
            cb(null, true);
        } else {
            // Reject file
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },
    // Storage properties
    storage: diskStorage({
        // Destination storage path details
        // !Notes:  file upload at the end.
        destination: async (req: any, file: any, cb: any) => {
            let uploadPath = '';

            uploadPath = join(__dirname, '../../', 'public/uploads');

            // Create folder if doesn't exist
            if (!existsSync(uploadPath)) {
                mkdirSync(uploadPath, { recursive: true });
            }
            cb(null, uploadPath);
        },
        // File modification details
        filename: (req: any, file: any, cb: any) => {
            // Calling the callback passing the random name generated with the original extension name

            cb(
                null,
                `${file.originalname.replace(/.jpg|.jpeg|.png|.gif/g, '-')}${moment(new Date()).format(
                    'YYYY-MM-DD'
                )}-${Math.random().toString(36).slice(2, 10)}-${extname(file.originalname)}`
            );
        },
    }),
};
