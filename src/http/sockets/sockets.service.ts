import { Injectable, Logger } from '@nestjs/common';
import { WsException } from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { verify } from 'jsonwebtoken';
import { ConfigService } from '@nestjs/config';
import { IAppConfig } from 'configs';

@Injectable()
export class ChatService {
    constructor(private readonly configService: ConfigService) {}

    private readonly logger = new Logger('ChatService');

    async handleConnection(socket: Socket) {
        this.logger.log(`Client connected: ${socket.id}`);
        try {
            const {
                auth: { appAccessTokenSecretKey },
            } = this.configService.get<IAppConfig>('commons');
            const token = socket.handshake.auth.token;
            const decode = verify(token, appAccessTokenSecretKey);
            console.log(decode);
        } catch (error) {
            socket.emit('unauthorized', { unauthorized: true });
            this.logger.error(error);
            socket.disconnect(true);
        }
    }

    handleDisconnect(socket: Socket) {
        try {
            this.logger.log(`Client disconnected: ${socket.id}`);
        } catch (error) {
            this.logger.error(error);
        }
    }
}
