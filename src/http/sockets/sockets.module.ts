import { Module } from '@nestjs/common';
import { Socket } from 'socket.io';
import { SocketsGateway } from './sockets.gateway';
import { ChatService } from './sockets.service';

@Module({
    providers: [SocketsGateway, ChatService],
})
export class SocketsModule {}
