import { Logger, UseFilters, UseGuards } from '@nestjs/common';
import {
    BaseWsExceptionFilter,
    ConnectedSocket,
    MessageBody,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    WsException,
    WsResponse,
} from '@nestjs/websockets';
import { WsGuard } from 'guards';
import { Server, Socket } from 'socket.io';
import { ChatService } from './sockets.service';

@UseFilters(new BaseWsExceptionFilter())
@WebSocketGateway(80, {
    cors: {
        origin: '*',
    },
})
export class SocketsGateway {
    constructor(private readonly chatService: ChatService) {}

    @WebSocketServer()
    server: Server;

    private readonly logger = new Logger('SocketsGateway');

    afterInit() {
        this.logger.log('SocketsGateway dependencies initialized');
    }

    // Filters aren't applied to connection handlers
    handleConnection(socket: Socket) {
        this.chatService.handleConnection(socket);
    }

    // Filters aren't applied to connection handlers
    handleDisconnect(socket: Socket) {
        this.chatService.handleDisconnect(socket);
    }

    @UseGuards(WsGuard)
    @SubscribeMessage('message')
    handleEvent(@ConnectedSocket() client, @MessageBody() data) {
        return data;
    }
}
