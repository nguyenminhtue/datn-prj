import { IsString } from 'class-validator';

export class RequestForgotPasswordDto {
    @IsString()
    token: string;

    @IsString()
    password: string;
}
