import { IsEmail, IsEnum } from 'class-validator';
import { MemberType } from 'enums';

export class RequestVerifyCodeDto {
    @IsEmail()
    email: string;

    @IsEnum(MemberType)
    type: number;
}
