import { IsString } from 'class-validator';

export class LoginTwitterDto {
    @IsString()
    accessToken: string;

    @IsString()
    accessTokenSecret: string;
}
