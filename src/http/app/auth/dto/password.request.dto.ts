import { IsNotEmpty, IsString } from 'class-validator';

export class PasswordRequestDto {
    @IsString()
    @IsNotEmpty({
        message: 'old password is required',
    })
    oldPassword: string;

    @IsString()
    @IsNotEmpty({
        message: 'new password is required',
    })
    newPassword: string;

    @IsString()
    @IsNotEmpty({
        message: 'confirm new password is required',
    })
    confirmNewPassword: string;
}
