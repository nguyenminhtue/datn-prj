import { IsString } from 'class-validator';

export class RequestUpdateUserdDto {
    @IsString()
    name: string;

    @IsString()
    description: string;

    @IsString()
    banner: string;

    @IsString()
    thumbnail: string;
}
