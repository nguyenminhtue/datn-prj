import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { AccessToken, Admin } from 'guards';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { PasswordRequestDto } from './dto/password.request.dto';
import { RegisterDto } from './dto/register.dto';
import { RequestForgotPasswordDto } from './dto/request-forgotPassword.dto';

@Controller('/auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('/register')
    async register(@Body() params: RegisterDto) {
        return await this.authService.register(params);
    }

    @Post('/login')
    async create(@Body() params: LoginDto) {
        return await this.authService.login(params);
    }

    @Post('/forgot-password')
    async forgotPassword(@Body() body: RequestForgotPasswordDto) {
        return await this.authService.forgotPassword(body);
    }

    @UseGuards(AccessToken)
    @Post('/change-password')
    async changePassword(@Req() req: Request, @Body() passwordRequest: PasswordRequestDto) {
        const { memberId } = req;
        return this.authService.changePassword(memberId, passwordRequest);
    }
}
