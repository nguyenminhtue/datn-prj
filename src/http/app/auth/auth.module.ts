import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { HttpModule } from '@nestjs/axios';
import { PassportModule } from '@nestjs/passport';
import { IAppConfig } from 'configs';
import { AccessTokenStrategy, AdminStrategy, PublicStrategy, RefreshTokenStrategy } from 'strategies';
import { MemberModule } from '../member/member.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { SendgridModule } from 'sendgrid/sendgrid.module';

@Module({
    imports: [
        PassportModule,
        SendgridModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => {
                const { appAccessTokenSecretKey, appAccessTokenExpiration } =
                    configService.get<IAppConfig>('commons').auth;
                return {
                    expiresIn: appAccessTokenExpiration,
                    secret: appAccessTokenSecretKey,
                };
            },
            inject: [ConfigService],
        }),
        MemberModule,
        HttpModule,
    ],
    controllers: [AuthController],
    providers: [
        AuthService,
        AccessTokenStrategy,
        RefreshTokenStrategy,
        PublicStrategy,
        AdminStrategy,
        // TwitterStrategy,
        // FacebookStrategy,
    ],
    exports: [AuthService],
})
export class AuthModule { }
