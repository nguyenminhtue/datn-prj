import { sign } from 'jsonwebtoken';

export async function createToken(body) {
    const appAccessTokenSecretKey = process.env.APP_ACCESS_TOKEN_SECRET_KEY;
    const appAccessTokenExpiration = process.env.APP_ACCESS_TOKEN_EXPIRATION;

    return sign({ ...body }, appAccessTokenSecretKey, { expiresIn: appAccessTokenExpiration });
}
