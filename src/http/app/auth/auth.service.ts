import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import to from 'await-to-js';
import { compareSync, hashSync } from 'bcrypt';
import { IAppConfig } from 'configs';
import { UserAccounts } from 'entities/user-accounts.entity';
import {
    ErrorMessage,
    MessageSendMail,
    StatusDeleteAccount,
    SuccessMessage,
    TypeSendMail,
} from 'enums';
import { verify } from 'jsonwebtoken';
import { pick } from 'lodash';
import { DataSource, DeepPartial, EntityManager } from 'typeorm';
import { promisify } from 'util';
import { LoginDto } from './dto/login.dto';
import { PasswordRequestDto } from './dto/password.request.dto';
import { RegisterDto } from './dto/register.dto';
import { RequestForgotPasswordDto } from './dto/request-forgotPassword.dto';
import { sendMail } from 'sendgrid/templates/template-send-mail';
import * as SendGrid from '@sendgrid/mail';
import { SendgridService } from 'sendgrid/sendgrid.service';
import { createToken } from './utils/create-token-public';

const verifyAsync = promisify(verify) as any;

export interface IVerifyOTPCodeParams {
    email: string;
    code: string;
    type: number;
}

export interface IRequestVerifyCode {
    email: string;
    type: number;
}

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
        private readonly dataSource: DataSource,
        private readonly sendgridService: SendgridService
    ) {
        SendGrid.setApiKey(this.configService.get<string>('SEND_GRID_KEY'));
    }

    async login(params: LoginDto) {
        const userAccountsRepo = this.dataSource.getRepository(UserAccounts);

        const member = await userAccountsRepo.findOne({
            where: { email: params.email, isDeleted: StatusDeleteAccount.NoDelete },
        });

        if (!member) {
            throw new HttpException(ErrorMessage.MemberNotExist, HttpStatus.BAD_REQUEST);
        }

        const isTrue = compareSync(params.password, member.password);
        if (!isTrue) {
            throw new HttpException(ErrorMessage.EmailOrPasswordIsInvalid, HttpStatus.BAD_REQUEST);
        }

        return await this.generateToken(member);
    }

    async forgotPassword(valueQuery: RequestForgotPasswordDto) {
        const { token, password } = valueQuery;
        const { saltOrRounds, appAccessTokenSecretKey } =
            this.configService.get<IAppConfig>('commons').auth;
        const userRepo = this.dataSource.getRepository(UserAccounts).createQueryBuilder('ua');
        try {
            const decode = verify(token, appAccessTokenSecretKey) as any;
            const user = await userRepo
                .where({ email: decode?.email })
                .andWhere({ token })
                .select(['ua.id'])
                .getOne();

            if (!user) throw new HttpException(ErrorMessage.EmailNotExist, HttpStatus.BAD_REQUEST);
            await userRepo
                .update(UserAccounts)
                .set({ password: hashSync(password, saltOrRounds), token: null })
                .where('id = :id', { id: user.id })
                .execute();
            return { message: SuccessMessage.ChangePasswordMessage };
        } catch (error) {
            throw new HttpException(ErrorMessage.ChangePassword, HttpStatus.BAD_REQUEST);
        }
    }

    async register(params: RegisterDto) {
        const { email, name, password } = params;
        const { saltOrRounds } = this.configService.get<IAppConfig>('commons').auth;
        const hashPass = hashSync(password, saltOrRounds);
        const userRepo = this.dataSource.getRepository(UserAccounts).createQueryBuilder();

        const checkNameUser = await userRepo
            .where({ name })
            .andWhere({ isDeleted: StatusDeleteAccount.NoDelete })
            .getOne();
        //check name exist
        if (checkNameUser) {
            throw new HttpException(ErrorMessage.UserNameExist, HttpStatus.BAD_REQUEST);
        }

        //check email exist
        const findUserExist = await userRepo
            .where({ email })
            .andWhere({ isDeleted: StatusDeleteAccount.NoDelete })
            .getOne();
        if (!findUserExist) {
            //check user deleted
            const checkUserDeleted = await userRepo
                .where({ email })
                .andWhere({ isDeleted: StatusDeleteAccount.Deleted })
                .getOne();
            // upadte username and password
            await userRepo
                .update(UserAccounts)
                .set({
                    name,
                    password: hashPass,
                    isDeleted: StatusDeleteAccount.NoDelete
                })
                .where({ email })
                .execute();

            if (!checkUserDeleted) {
                await userRepo
                    .insert()
                    .into(UserAccounts)
                    .values({
                        name,
                        email,
                        password: hashPass,
                        isDeleted: StatusDeleteAccount.NoDelete,
                    })
                    .execute();
            }
        } else {
            throw new HttpException(ErrorMessage.EmailAlreadyExist, HttpStatus.BAD_REQUEST);
        }
        return {
            messagge: SuccessMessage.CreateAccount,
        };
    }

    async generateToken(params: DeepPartial<UserAccounts>, transaction?: EntityManager) {
        const userAccountsRepo = transaction
            ? transaction.getRepository(UserAccounts)
            : this.dataSource.getRepository(UserAccounts);

        const { appRefreshTokenSecretKey } = this.configService.get<IAppConfig>('commons').auth;

        const payload = pick(params, ['id', 'email', 'isDeleted', 'roleId']);

        const token = this.generateAccessToken(payload);
        const oldRefreshToken = params.refreshToken;

        const [error] = await to(verifyAsync(oldRefreshToken, appRefreshTokenSecretKey));
        if (error) {
            const newRefreshToken = this.generateRefreshToken(payload);
            await userAccountsRepo.update({ id: params?.id }, { refreshToken: newRefreshToken });
            return { token, refreshToken: newRefreshToken };
        }

        return { token, refreshToken: oldRefreshToken };
    }

    generateAccessToken(params) {
        const { appAccessTokenSecretKey, appAccessTokenExpiration } =
            this.configService.get<IAppConfig>('commons').auth;

        return this.jwtService.sign(params, {
            expiresIn: appAccessTokenExpiration,
            secret: appAccessTokenSecretKey,
        });
    }

    generateRefreshToken(params) {
        const { appRefreshTokenSecretKey, appRefreshTokenExpiration } =
            this.configService.get<IAppConfig>('commons').auth;

        return this.jwtService.sign(params, {
            expiresIn: appRefreshTokenExpiration,
            secret: appRefreshTokenSecretKey,
        });
    }

    async changePassword(memberId: number, passwordRequest: PasswordRequestDto) {
        const { oldPassword, newPassword, confirmNewPassword } = passwordRequest;
        const memberRepo = this.dataSource.getRepository(UserAccounts).createQueryBuilder('user');
        const { saltOrRounds } = this.configService.get<IAppConfig>('commons').auth;
        const getUser = await memberRepo
            .select(['user.id', 'user.email', 'user.password'])
            .where({
                id: memberId,
            })
            .getOne();

        if (!getUser) {
            throw new HttpException(ErrorMessage.MemberNotExist, HttpStatus.BAD_REQUEST);
        }

        const isTrue = compareSync(oldPassword, getUser.password);
        if (!isTrue) {
            throw new HttpException(ErrorMessage.OldPasswordIsInvalid, HttpStatus.BAD_REQUEST);
        }

        if (newPassword !== confirmNewPassword) {
            throw new HttpException(ErrorMessage.PassWordNotCompare, HttpStatus.BAD_REQUEST);
        }

        const hashPass = hashSync(newPassword, saltOrRounds);

        await memberRepo.update().set({ password: hashPass }).where({ id: memberId }).execute();

        return {
            message: SuccessMessage.ChangePasswordMessage,
        };
    }

    async update(valueQuery) {
        const { memberId, name, description, banner, thumbnail } = valueQuery;

        const result = await this.dataSource
            .getRepository(UserAccounts)
            .createQueryBuilder()
            .update(UserAccounts)
            .set({
                name,
                description,
                banner,
                thumbnail,
            })
            .where({ id: memberId })
            .execute();

        if (result) return { message: SuccessMessage.UpdateMessage };
        else throw new HttpException(ErrorMessage.UpdateMessage, HttpStatus.BAD_REQUEST);
    }
}
