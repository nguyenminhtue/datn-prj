import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put,
    Query,
    Req,
    UploadedFile,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { AccessToken } from 'guards';
import { Request } from 'express';
import { Public } from 'guards/public.guard';
import { MemberService } from './member.service';
import { InfoMemberRequest } from './dto/member.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('/member')
export class MemberController {
    constructor(private readonly memberService: MemberService) { }
    @UseGuards(AccessToken)
    @Get('/list')
    async getMembers(@Req() req: Request, @Query() query) {
        return await this.memberService.getMember(query);
    }

    @UseGuards(AccessToken)
    @Get('my-profile')
    async getProfileUserLogin(@Req() req: Request) {
        const { memberId } = req;
        return await this.memberService.getProfileUserLogin(memberId);
    }

    @UseGuards(Public)
    @Get('/:id')
    async getMyProfile(@Param('id', ParseIntPipe) id: number) {
        return await this.memberService.getMemberInfoById(id);
    }

    @UseGuards(AccessToken)
    @Post('/')
    async createMember(@Body() body) {
        return await this.memberService.createMember(body);
    }

    @UseGuards(AccessToken)
    @Delete('/:id')
    async deleteMember(@Param('id', ParseIntPipe) id: number) {
        return await this.memberService.deleteMember(id);
    }

    @UseGuards(AccessToken)
    @Put('/edit-info')
    async editInfoMember(@Req() req: Request, @Body() info: InfoMemberRequest) {
        const reqUser = req.user as any;
        const id = reqUser?.id;
        return await this.memberService.editInfoMember(id, info);
    }

    @UseGuards(AccessToken)
    @Put('/edit-image')
    @UseInterceptors(FileInterceptor('file'))
    async editImageMember(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
        @Body('type', ParseIntPipe) type: number
    ) {
        const { memberId } = req;
        return await this.memberService.editImageMember(memberId, file, type);
    }
}
