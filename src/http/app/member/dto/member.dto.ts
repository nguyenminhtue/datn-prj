import { IsOptional, IsString } from 'class-validator';

export class InfoMemberRequest {
    @IsOptional()
    @IsString()
    name?: string;

    @IsOptional()
    @IsString()
    description?: string;
}
