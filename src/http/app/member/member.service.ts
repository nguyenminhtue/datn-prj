import { BlobServiceClient } from '@azure/storage-blob';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Roles } from 'entities/roles.entity';
import { UserAccounts } from 'entities/user-accounts.entity';
import {
    ContainerAzue,
    ErrorMessage,
    StatusDeleteAccount,
    SuccessMessage,
    TypeImageUser,
} from 'enums';
import { handleResPagination } from 'helpers';
import { DataSource, EntityManager, Not } from 'typeorm';
import { InfoMemberRequest } from './dto/member.dto';

@Injectable()
export class MemberService {
    constructor(private readonly dataSource: DataSource) { }

    async getMyProfile(id: number) {
        const userAccountsRepo = this.dataSource.getRepository(UserAccounts);

        const qb = userAccountsRepo
            .createQueryBuilder('ua')
            .select(['ua.id', 'ua.name', 'ua.description', 'ua.banner', 'ua.thumbnail'])
            .where('1=1')
            .andWhere('ua.id = :id', { id });

        const result = await qb.getOne();
        return result;
    }

    async getMemberInfoById(memberId: number, transaction?: EntityManager) {
        const userAccountsRepo = transaction
            ? transaction.getRepository(UserAccounts)
            : this.dataSource.getRepository(UserAccounts);
        return await userAccountsRepo
            .createQueryBuilder('ua')
            .select(['ua.id', 'ua.name', 'ua.description', 'ua.banner', 'ua.thumbnail'])
            .where('1=1')
            .andWhere('ua.id = :memberId', { memberId })
            .getOne();
    }

    async getMemberInfoByEmail(email: string, transaction?: EntityManager) {
        const userAccountsRepo = transaction
            ? transaction.getRepository(UserAccounts)
            : this.dataSource.getRepository(UserAccounts);
        return await userAccountsRepo.findOne({ where: { email } });
    }

    async getMember(query) {
        const { search } = query
        const userRepo = this.dataSource.getRepository(UserAccounts).createQueryBuilder('user').where({ roleId: 2 })
            .andWhere({ isDeleted: 0 })
        if (search) {
            userRepo.andWhere(
                `((user.name LIKE :search) OR (user.email LIKE :search))`, { search: `%${search}%` }
            )
        }
        return await userRepo.getMany()

    }

    async deleteMember(id: number) {
        await this.dataSource
            .getRepository(UserAccounts)
            .update({ id }, { isDeleted: StatusDeleteAccount.Deleted });

        return {
            message: 'Xóa user thành công',
        };
    }

    async editInfoMember(memberId: number, info: InfoMemberRequest) {
        const userRepo = this.dataSource.getRepository(UserAccounts);
        const infoMember = await userRepo.createQueryBuilder().where({ id: memberId }).getOne();

        if (!infoMember) {
            throw new HttpException(ErrorMessage.MemberNotExist, HttpStatus.BAD_REQUEST);
        }

        const data: InfoMemberRequest = info;

        // update name or description
        await userRepo.update({ id: memberId }, data);
        return {
            message: SuccessMessage.UpdateMessage,
        };
    }

    async editImageMember(memberId: number, file, type: number) {
        try {
            const userRepo = this.dataSource.getRepository(UserAccounts);
            const infoMember = await userRepo.createQueryBuilder().where({ id: memberId }).getOne();

            if (!infoMember) {
                throw new HttpException(ErrorMessage.MemberNotExist, HttpStatus.BAD_REQUEST);
            }

            const blobClientService = BlobServiceClient.fromConnectionString(
                `${process.env.AZURE_STORAGE_CONNECTION_STRING}`
            );
            const containerClient = blobClientService.getContainerClient(ContainerAzue.Accounts);
            // upload
            const fileName = `${Date.now().toString()}-${file.originalname}`;
            const blobClient = containerClient.getBlockBlobClient(fileName);

            await blobClient.uploadData(file.buffer);

            if (type === TypeImageUser.Thumbnail) {
                await userRepo.update({ id: memberId }, { thumbnail: blobClient?.url });
            }
            if (type === TypeImageUser.Banner) {
                await userRepo.update({ id: memberId }, { banner: blobClient?.url });
            }
            return {
                message: SuccessMessage.UpdateMessage,
            };
        } catch {
            throw new HttpException(ErrorMessage.UpdateMessage, HttpStatus.BAD_REQUEST);
        }
    }

    async getProfileUserLogin(memberId: number) {
        const user = await this.dataSource
            .getRepository(UserAccounts)
            .createQueryBuilder('user')
            .leftJoinAndMapOne('user.roles', Roles, 'role', 'user.roleId = role.id')
            .select([
                'user.name',
                'user.email',
                'user.banner',
                'user.thumbnail',
                'role.id',
                'role.name',
            ])
            .where({ id: memberId })
            .getOne();

        if (!user) throw new HttpException(ErrorMessage.EmailNotExist, HttpStatus.BAD_REQUEST);
        return user;
    }

    async createMember(body) {
        const { email } = body
        const userRepo = this.dataSource.getRepository(UserAccounts)
        const user = await userRepo.findOne({ where: { email } })
        if (user) {
            throw new HttpException('Email bị trùng, vui lòng sử dụng email khác', HttpStatus.BAD_REQUEST);
        }
        await userRepo.insert(body)
        return {
            message: 'Đăng ký thành công User'
        }
    }
}
