import { IsNumber, IsOptional, IsString } from "class-validator";

export class CreateCounpon {
    @IsString()
    @IsOptional()
    code: string;

    @IsNumber()
    @IsOptional()
    couponPersent: number;

    @IsString()
    @IsOptional()
    expriedAt: string | Date;
}
