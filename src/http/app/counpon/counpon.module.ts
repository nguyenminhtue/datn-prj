import { Module } from '@nestjs/common';
import { CounponController } from './counpon.controller';
import { CounponService } from './counpon.service';

@Module({
  controllers: [CounponController],
  providers: [CounponService]
})
export class CounponModule {}
