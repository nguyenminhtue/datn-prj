import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query } from '@nestjs/common';
import { CounponService } from './counpon.service';
import { CreateCounpon } from './dto/create-counpon.dto';

@Controller('counpon')
export class CounponController {
    constructor(private readonly counponService: CounponService) { }

    // get list counpon
    @Get('/')
    async getListCounpon(@Query() query) {
        return await this.counponService.getListCounpon(query);
    }

    // create counpon
    @Post('/')
    async createCounpon(@Body() body: CreateCounpon) {
        return await this.counponService.createCounpon(body);
    }

    //update counpon
    @Put('/:id')
    async updateCoupon(@Param('id', ParseIntPipe) id: number, @Body() body: CreateCounpon) {
        return await this.counponService.updateCoupon(id, body);
    }

    //delete counpon
    @Delete('/:id')
    async deleteCoupon(@Param('id', ParseIntPipe) id: number) {
        return await this.counponService.deleteCoupon(id);
    }

}
