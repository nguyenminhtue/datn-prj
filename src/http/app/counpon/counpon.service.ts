import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Coupons } from 'entities/counpon.entity';
import { DataSource } from 'typeorm';
import { CreateCounpon } from './dto/create-counpon.dto';

@Injectable()
export class CounponService {
    constructor(
        private readonly dataSource: DataSource,
    ) { }
    async getListCounpon(query) {
        const { status } = query
        const result = await this.dataSource.getRepository(Coupons).find({
            where: {
                isDeleted: 0
            }
        }) as any
        if (result && result.length > 0) {
            for (const item of result) {
                if (item.expriedAt < new Date()) {
                    item.canUse = 0
                } else {
                    item.canUse = 1
                }
            }
        }
        if (status && Number(status) === 0) {
            return result.filter(item =>
                item.canUse === 0
            )
        }
        if (status && Number(status) === 1) {
            return result.filter(item =>
                item.canUse === 1
            )
        }
        return result
    }

    async createCounpon(body: CreateCounpon) {
        const { code, couponPersent, expriedAt } = body
        console.log(1111, body);
        const checkCouponExist = await this.dataSource.getRepository(Coupons).findOne({
            where: {
                code,
                isDeleted: 0
            }
        })
        if (checkCouponExist) {
            throw new HttpException('Mã giảm giá đã bị trùng', HttpStatus.BAD_REQUEST);
        }
        await this.dataSource.getRepository(Coupons).insert(body)
        return {
            message: "Thêm mới coupon thành công"
        }
    }

    async updateCoupon(id: number, body: CreateCounpon) {
        await this.dataSource.getRepository(Coupons).update({ id }, body)
        return {
            message: "Cập nhật coupon thành công"
        }
    }

    async deleteCoupon(id: number) {
        await this.dataSource.getRepository(Coupons).update({ id }, { isDeleted: 1 })
        return {
            message: "Xóa coupon thành công"
        }
    }
}
