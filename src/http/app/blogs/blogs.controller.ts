import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Admin } from 'guards';
import { multerOptions } from 'untils/functions';
import { BlogsService } from './blogs.service';
import { BlogDto } from './dto/create-blogs.dto';

@Controller('blogs')
export class BlogsController {
    constructor(private readonly blogsService: BlogsService) { }

    // get list counpon
    @Get('/')
    async getListCounpon() {
        return await this.blogsService.getListBlogs();
    }

    //create blogs
    @UseGuards(Admin)
    @UseInterceptors(FileInterceptor('file', multerOptions))
    @Post('/')
    async createBlogs(@Req() req, @Body() body: BlogDto, @UploadedFile() file: Express.Multer.File) {
        return await this.blogsService.createBlogs(req.memberId, body, file)
    }

    //update blogs
    @UseGuards(Admin)
    @UseInterceptors(FileInterceptor('file', multerOptions))
    @Put('/:id')
    async updateBlogs(@Param('id', ParseIntPipe) id: number, @Body() body: BlogDto, @UploadedFile() file: Express.Multer.File) {
        return await this.blogsService.updateBlogs(id, body, file)
    }

    //delete blogs
    @UseGuards(Admin)
    @Delete('/:id')
    async deleteBlogs(@Param('id', ParseIntPipe) id: number) {
        return await this.blogsService.deleteBlogs(id)
    }
}
