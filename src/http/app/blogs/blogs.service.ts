import { Injectable } from '@nestjs/common';
import { Blogs } from 'entities/blogs.entity';
import { UserAccounts } from 'entities/user-accounts.entity';
import { DataSource } from 'typeorm';
import { BlogDto } from './dto/create-blogs.dto';

@Injectable()
export class BlogsService {
    constructor(
        private readonly dataSource: DataSource,
    ) { }

    async getListBlogs() {
        return await this.dataSource.getRepository(Blogs).createQueryBuilder('blogs')
            .leftJoinAndMapOne('blogs.user', UserAccounts, 'user', 'user.id = blogs.userId')
            .getRawMany()
    }

    async createBlogs(userId: number, body: BlogDto, file: any) {
        await this.dataSource.getRepository(Blogs).insert({
            ...body,
            userId,
            thumbnail: file.filename
        })
        return {
            message: "Thêm blogs thành công"
        }
    }

    async updateBlogs(id: number, body: BlogDto, file: any) {
        await this.dataSource.getRepository(Blogs).update({ id }, {
            ...body,
            thumbnail: file.filename
        })
        return {
            message: "Update blogs thành công"
        }
    }

    async deleteBlogs(id: number) {
        await this.dataSource.getRepository(Blogs).update({ id }, { isDeleted: 1 })
        return {
            message: "Delete blogs thành công"
        }
    }
}
