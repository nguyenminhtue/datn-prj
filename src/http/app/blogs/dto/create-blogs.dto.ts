import { IsNumber, IsOptional, IsString } from "class-validator";

export class BlogDto {
    @IsString()
    @IsOptional()
    title: string;

    @IsString()
    @IsOptional()
    content: string;

    @IsString()
    @IsOptional()
    thumbnail: string;
}
