import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { BookingRooms } from 'entities/booking-rooms.entity';
import { Bookings } from 'entities/bookings.entity';
import { Coupons } from 'entities/counpon.entity';
import { Customers } from 'entities/customers.entity';
import { Rooms } from 'entities/rooms.entity';
import { ErrorMessage, SuccessMessage } from 'enums';
import * as moment from 'moment';
import { Between, DataSource, In, IsNull, LessThan, MoreThan, Not } from 'typeorm';
import { CustomersService } from '../customers/customers.service';
import { changeStatusBooking } from './dto/change-status-booking.dto';
import { BookingDto } from './dto/create-booking.dto';
import { RequestBooking } from './dto/request-booking.dto';
import * as SendGrid from '@sendgrid/mail';
import * as _ from 'lodash'

@Injectable()
export class BookingsService {
    constructor(
        private readonly configService: ConfigService,
        private readonly customerService: CustomersService,
        private readonly dataSource: DataSource,
    ) {
        SendGrid.setApiKey(this.configService.get<string>('SEND_GRID_KEY'));
    }
    async getListBooking(query: RequestBooking) {
        const { status, take, page } = query;
        const limitResult = Number(take) || 10;
        const skipResult = Number(page) || 1;
        const skip = (skipResult - 1) * limitResult;

        const bookingRepo = this.dataSource.getRepository(Bookings).createQueryBuilder('booking')
        if (status) {
            bookingRepo.where({ status: Number(status) })
        }
        const [data, total] = await bookingRepo
            .leftJoinAndMapMany('booking.bookingRooms', BookingRooms, 'rooms', 'rooms.bookingId = booking.id')
            .leftJoinAndMapOne('rooms.room', Rooms, 'r', 'r.id = rooms.roomId')
            .leftJoinAndMapOne('booking.customer', Customers, 'cus', 'cus.id = booking.customerId')
            .andWhere({ isDeleted: 0 })
            .take(limitResult)
            .skip(skip)
            .orderBy('booking.id', 'DESC')
            .getManyAndCount();
        return {
            data,
            total,
            page: Number(page) || 1,
            take: limitResult,
        };
    }

    async createBooking(data: BookingDto) {
        const { email, name, phoneNumber, idCard, checkinDate, checkoutDate, status, code, child, adult } = data
        const maxPeopleARoom = 4
        //create customer
        const dataCreateCustomer = { email, name, phoneNumber, idCard }
        const customer = await this.customerService.createCustomer(dataCreateCustomer)
        const customerId = customer.userId

        const checkBooking = await this.dataSource.getRepository(Bookings).findOne({ where: { status: 1, customerId } })

        if (checkBooking) {
            throw new HttpException('Tồn tại 1 lịch book của khách hàng chưa thanh toán', HttpStatus.BAD_REQUEST);
        }
        let couponId = null
        let counpon: any
        if (code) {
            counpon = await this.dataSource.getRepository(Coupons).findOne({ where: { code, isDeleted: 0 } })
            if (counpon && counpon.expriedAt > new Date()) {
                couponId = counpon.id
            }
        }

        //check room exist
        const getBooking1 = await this.dataSource.getRepository(Bookings)
            .createQueryBuilder('booking')
            .where({ status: Not(3) })
            .andWhere({ isDeleted: 0 })
            .andWhere(`(booking.checkinDate BETWEEN '${moment(new Date(checkinDate)).format('YYYY-MM-DD HH:mm:ss')}' AND '${moment(new Date(checkoutDate)).format('YYYY-MM-DD HH:mm:ss')}')`)
            .select(['booking.id'])
            .getMany()

        const getBooking2 = await this.dataSource.getRepository(Bookings)
            .createQueryBuilder('booking')
            .where({ status: Not(3) })
            .andWhere({ isDeleted: 0 })
            .andWhere(`(booking.checkoutDate BETWEEN '${moment(new Date(checkinDate)).format('YYYY-MM-DD HH:mm:ss')}' AND '${moment(new Date(checkoutDate)).format('YYYY-MM-DD HH:mm:ss')}')`)
            .select(['booking.id'])
            .getMany()

        const getBooking3 = await this.dataSource.getRepository(Bookings)
            .createQueryBuilder('booking')
            .where({ status: Not(3) })
            .andWhere({ isDeleted: 0 })
            .andWhere(`(('${moment(new Date(checkinDate)).format('YYYY-MM-DD HH:mm:ss')}' BETWEEN booking.checkinDate AND '${moment(new Date(checkoutDate)).format('YYYY-MM-DD HH:mm:ss')}')
             AND ('${moment(new Date(checkoutDate)).format('YYYY-MM-DD HH:mm:ss')}' BETWEEN '${moment(new Date(checkinDate)).format('YYYY-MM-DD HH:mm:ss')}' AND booking.checkoutDate))`)
            .select(['booking.id'])
            .getMany()

        const getBooking = getBooking1.concat(getBooking2).concat(getBooking3)
        const listBookingIds = []
        getBooking.length > 0 && getBooking.map(item => {
            listBookingIds.push(item?.id)
        })

        //get list room da duoc book
        let roomExist = []
        if (listBookingIds.length > 0) {
            roomExist = await this.dataSource.getRepository(BookingRooms).createQueryBuilder('br')
                .where(`br.bookingId IN (:...listBookingIds)`, { listBookingIds: listBookingIds })
                .getMany()
        }

        const listRoomExist = []
        roomExist.length > 0 && roomExist.map(item => {
            listRoomExist.push(item.roomId)
        })

        const arrayRoomAlready = []
        const listRoomAlready = await this.dataSource.getRepository(Rooms).createQueryBuilder('room')
            .where({ id: Not(In(listRoomExist)) })
            .andWhere({ type: 1 })
            .select(['room.id'])
            .getMany()
        listRoomAlready.map(item => {
            if (!arrayRoomAlready.includes(item.id)) {
                arrayRoomAlready.push(item.id)
            }
        })

        //count people 
        let totalPeople = 0
        totalPeople += Number(adult)
        if (child) {
            totalPeople += Math.floor(Number(child) / 2)
        }

        //tinh so phong can dat
        let rooms = 1
        if (totalPeople > 4) {
            rooms = Math.ceil(totalPeople / maxPeopleARoom)
        }

        if (arrayRoomAlready.length < rooms) {
            throw new HttpException('Không đủ số phòng trống', HttpStatus.BAD_REQUEST);
        }
        const arrayRoomIdInsert = arrayRoomAlready.splice(0, rooms)

        const dataBooking = { checkinDate, checkoutDate, status, customerId, couponId }
        const bookings = await this.dataSource.getRepository(Bookings).insert(dataBooking)
        for (const item of arrayRoomIdInsert) {
            await this.dataSource.getRepository(BookingRooms).insert({
                bookingId: bookings.identifiers[0].id,
                roomId: item
            })
        }

        await this.dataSource.getRepository(Coupons).update({ code }, { isDeleted: 1 })
        //send email
        const arrRoomName = []
        let price = 0
        let couponMoney = 0
        for (const idx of arrayRoomIdInsert) {
            const room = await this.dataSource.getRepository(Rooms).findOne({ where: { id: idx } })
            price = price + room.price
            arrRoomName.push(room?.name)
        }
        const date1 = moment(checkinDate)
        const date2 = moment(checkoutDate)
        var diffInHours = date2.diff(date1, 'hours');
        price = Math.round(price * diffInHours / 24)
        if (counpon) {
            price = Math.round(price * counpon?.couponPersent / 100)
        }

        await this.dataSource.getRepository(Bookings).update({ id: bookings.identifiers[0].id }, { totalRate: price })

        const templateId = 'd-f2441bc0efd84e53afeb86981ae4c56f'
        const dataSendMail = {
            from: process.env.SEND_GRID_EMAIL,
            personalizations: [
                {
                    "to": [
                        {
                            "email": email
                        }
                    ],
                    dynamicTemplateData: {
                        "checkinDate": checkinDate,
                        "checkoutDate": checkoutDate,
                        "listRoom": arrRoomName.toString(),
                        "price": price,
                        "deposit": price * 20 / 100,
                    }
                },
            ],
            templateId
        }

        SendGrid.send(dataSendMail)
            .then(async () => {
            })
            .catch(() => {
                throw new HttpException(ErrorMessage.BadRequest, HttpStatus.BAD_REQUEST);
            });

        return {
            message: 'Tạo mới lịch booking thành công. Quý khách vui lòng kiểm tra email để xác nhận đặt phòng!'
        }
    }

    async deleteBooking(id: number, email: string) {
        const data = await this.dataSource.getRepository(Bookings).createQueryBuilder('booking')
            .leftJoinAndMapMany('booking.bookingRooms', BookingRooms, 'rooms', 'rooms.bookingId = booking.id')
            .leftJoinAndMapOne('rooms.room', Rooms, 'r', 'r.id = rooms.roomId')
            .leftJoinAndMapOne('booking.customer', Customers, 'cus', 'cus.id = booking.customerId')
            .where({ id })
            .andWhere({ status: 1 })
            .orderBy('booking.id', 'DESC')
            .getOne() as any

            if (data) {
                const templateId = 'd-4e8b7dcbe967454cb2800d45df5dc02d'
                const dataSendMail = {
                    from: process.env.SEND_GRID_EMAIL,
                    personalizations: [
                        {
                            "to": [
                                {
                                    "email": email
                                }
                            ]
                        },
                    ],
                    templateId
                }
        
                SendGrid.send(dataSendMail)
                    .then(async () => {
                    })
                    .catch((error) => {
                        throw new HttpException(ErrorMessage.BadRequest, HttpStatus.BAD_REQUEST);
                    });
            }
        await this.dataSource.getRepository(Bookings).update({ id }, { isDeleted: 1 })
        return {
            message: 'Xóa booking thành công'
        }
    }

    async updateBooking(id: number, body: BookingDto) {
        const { status } = body
        const bookingRepo = this.dataSource.getRepository(Bookings)
        //check booking exist
        const booking = await bookingRepo.findOne({ where: { id } })
        if (!booking) {
            throw new HttpException('Không tồn tại booking', HttpStatus.BAD_REQUEST);
        }
        await bookingRepo.update({ id }, body)
        const data = await bookingRepo.createQueryBuilder('booking')
            .leftJoinAndMapMany('booking.bookingRooms', BookingRooms, 'rooms', 'rooms.bookingId = booking.id')
            .leftJoinAndMapOne('rooms.room', Rooms, 'r', 'r.id = rooms.roomId')
            .leftJoinAndMapOne('booking.customer', Customers, 'cus', 'cus.id = booking.customerId')
            .where({ isDeleted: 0 })
            .andWhere({ id })
            .orderBy('booking.id', 'DESC')
            .getOne() as any
        if (status === 2) {
            const templateId = 'd-df69108ff4f04ada85a64b1fe805713f'
            const dataSendMail = {
                from: process.env.SEND_GRID_EMAIL,
                personalizations: [
                    {
                        "to": [
                            {
                                "email": data?.customer?.email
                            }
                        ]
                    },
                ],
                templateId
            }

            SendGrid.send(dataSendMail)
                .then(async () => {
                })
                .catch((error) => {
                    throw new HttpException(ErrorMessage.BadRequest, HttpStatus.BAD_REQUEST);
                });

            const arrRoomId = []
            const listRoom = await this.dataSource.getRepository(BookingRooms).find({
                where: {
                    bookingId: id
                },
                select: ['roomId']
            })
            listRoom.map(room => {
                arrRoomId.push(room.roomId)
            })
            if (arrRoomId.length > 0) {
                await this.dataSource.getRepository(Rooms).createQueryBuilder('rooms')
                    .update()
                    .set({
                        isBooked: 1
                    })
                    .where(`rooms.id IN (:...arrRoomId)`, { arrRoomId: arrRoomId })
                    .execute();
            }
        }

        if (status === 3) {
            const arrRoomId = []
            const listRoom = await this.dataSource.getRepository(BookingRooms).find({
                where: {
                    bookingId: id
                },
                select: ['roomId']
            })
            listRoom.map(room => {
                arrRoomId.push(room.roomId)
            })
            if (arrRoomId.length > 0) {
                await this.dataSource.getRepository(Rooms).createQueryBuilder('rooms')
                    .update()
                    .set({
                        isBooked: 0
                    })
                    .where(`rooms.id IN (:...arrRoomId)`, { arrRoomId: arrRoomId })
                    .execute();
            }
            await this.dataSource.getRepository(Bookings).update({ id }, { updatedAt: moment(new Date()).format('YYYY-MM-DD HH:mm:ss') })
        }
        return {
            message: 'Update booking thành công'
        }
    }

    async getListDoanhThu() {
        const result = await this.dataSource.getRepository(Bookings).find({
            where: {
                status: 3
            }

        }) as any

        let totalMoney = 0
        result.map(item => {
            totalMoney += item.totalRate
            item.month = moment(item.checkoutDate).format('YYYY-MM')
        })

        const totalBooked = await this.dataSource.getRepository(Bookings).count({})

        const totalCustomer = await this.dataSource.getRepository(Customers).count()

        const totalRoom = await this.dataSource.getRepository(Rooms).count()

        let grouped = _.mapValues(_.groupBy(result, 'month'),
            clist => clist.map(car => _.omit(car, 'month')));

        // console.log(grouped);
        let listRate = [] as any
        for (const property in grouped) {
            let money = 0
            grouped[property].map(i => {
                money += i.totalRate
            })
            listRate.push({
                month: property,
                totalMoney: money
            })
        }
        listRate.sort((a: any, b: any) => new Date(b.month + '-01 00:00:00').getTime() - new Date(a.month + '-01 00:00:00').getTime())
        return {
            totalMoney,
            totalBooked,
            totalCustomer,
            totalRoom,
            listRate
        }
    }
}
