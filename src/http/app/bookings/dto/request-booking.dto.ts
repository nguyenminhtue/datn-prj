import { IsOptional, IsString } from 'class-validator';

export class RequestBooking {
    @IsString()
    @IsOptional()
    status: string;

    @IsString()
    @IsOptional()
    take: string;

    @IsString()
    @IsOptional()
    page: string;
}
