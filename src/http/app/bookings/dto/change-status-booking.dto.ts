import { IsNumber, IsOptional } from 'class-validator';

export class changeStatusBooking {
    @IsNumber()
    @IsOptional()
    status: number;

    @IsNumber()
    @IsOptional()
    deposit: number;
}
