import { IsInt, IsNumber, IsOptional, IsString } from "class-validator";

export class BookingDto {
    @IsString()
    @IsOptional()
    email: string;

    @IsString()
    @IsOptional()
    name: string;

    @IsString()
    @IsOptional()
    phoneNumber: string;

    @IsNumber()
    @IsOptional()
    deposit: number;

    @IsString()
    @IsOptional()
    idCard: string;

    @IsString()
    @IsOptional()
    checkinDate: Date | string;

    @IsString()
    @IsOptional()
    checkoutDate: Date | string;

    @IsInt()
    @IsOptional()
    status: number;

    @IsOptional()
    code?: string;

    @IsOptional()
    child?: number;

    @IsOptional()
    adult?: number;
}
