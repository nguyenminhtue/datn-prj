import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, UseGuards } from '@nestjs/common';
import { Admin } from 'guards';
import { BookingsService } from './bookings.service';
import { BookingDto } from './dto/create-booking.dto';
import { RequestBooking } from './dto/request-booking.dto';

@Controller('/bookings')
export class BookingsController {
    constructor(private readonly bookingsService: BookingsService) { }

    // get list bookings
    @UseGuards(Admin)
    @Get('/')
    async getListBooking(@Query() query: RequestBooking) {
        return await this.bookingsService.getListBooking(query);
    }

    // create booking
    @Post('/')
    async createBooking(@Body() body: BookingDto) {
        return await this.bookingsService.createBooking(body);
    }

    //update booking
    @Put('/:id')
    async updateBooking(@Param('id', ParseIntPipe) id: number, @Body() body: BookingDto) {
        return await this.bookingsService.updateBooking(id, body);
    }

    //delete booking
    @UseGuards(Admin)
    @Put('/delete/:id')
    async deleteBooking(@Param('id', ParseIntPipe) id: number, @Body('email') email: string) {
        return await this.bookingsService.deleteBooking(id, email);
    }

    //quan ly doanh thu
    @Get('/doanh-thu')
    async getListDoanhThu() {
        return await this.bookingsService.getListDoanhThu();
    }
}
