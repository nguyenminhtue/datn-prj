import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { IAppConfig } from 'configs';
import { CustomersModule } from '../customers/customers.module';
import { BookingsController } from './bookings.controller';
import { BookingsService } from './bookings.service';

@Module({
  imports: [CustomersModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        const { appAccessTokenSecretKey, appAccessTokenExpiration } =
          configService.get<IAppConfig>('commons').auth;
        return {
          expiresIn: appAccessTokenExpiration,
          secret: appAccessTokenSecretKey,
        };
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [BookingsController],
  providers: [BookingsService]
})
export class BookingsModule { }
