import { Module } from '@nestjs/common';
import { CleansController } from './cleans.controller';
import { CleansService } from './cleans.service';

@Module({
  controllers: [CleansController],
  providers: [CleansService]
})
export class CleansModule {}
