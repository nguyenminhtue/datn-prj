import { Body, Controller, Get, Param, ParseIntPipe, Post, Put, Query } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomerDto } from './dto/create-customer.dto';
import { RequestCustomer } from './dto/request-customer.dto';

@Controller('customers')
export class CustomersController {
    constructor(private readonly customersService: CustomersService) { }

    @Get('/')
    async getListCustomer(@Query() query: RequestCustomer) {
        return await this.customersService.getListCustomer(query);
    }

    @Post('/')
    async createCustomer(@Body() body: CustomerDto) {
        return await this.customersService.createCustomer(body);
    }

    @Put('/:id')
    async updateCustomer(@Param('id', ParseIntPipe) id: number, @Body() body: CustomerDto) {
        return await this.customersService.updateCustomer(id, body);
    }
}
