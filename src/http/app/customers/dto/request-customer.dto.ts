import { IsOptional, IsString } from 'class-validator';

export class RequestCustomer {
    @IsString()
    @IsOptional()
    take: string;

    @IsString()
    @IsOptional()
    page: string;

    @IsString()
    @IsOptional()
    search: string;
}
