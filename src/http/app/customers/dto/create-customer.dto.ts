import { IsInt, IsOptional, IsString } from "class-validator";

export class CustomerDto {
    @IsString()
    @IsOptional()
    email: string;

    @IsString()
    @IsOptional()
    name: string;

    @IsString()
    @IsOptional()
    phoneNumber: string;

    @IsString()
    @IsOptional()
    idCard: string;
}
