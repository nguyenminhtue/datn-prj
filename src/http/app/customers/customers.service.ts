import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CustomerDto } from './dto/create-customer.dto';
import { DataSource, Not } from 'typeorm';
import { Customers } from 'entities/customers.entity';
import { RequestCustomer } from './dto/request-customer.dto';

@Injectable()
export class CustomersService {
    constructor(
        private readonly dataSource: DataSource,
    ) {
    }

    async getListCustomer(query: RequestCustomer) {
        const { take, page, search } = query;
        const limitResult = Number(take) || 10;
        const skipResult = Number(page) || 1;
        const skip = (skipResult - 1) * limitResult;

        const customers = this.dataSource.getRepository(Customers).createQueryBuilder('cus')
        if (search) {
            customers.where(`((cus.name LIKE :search) OR (cus.email LIKE :search))`, { search: `%${search}%` })
        }
        const [data, total] = await customers
            .take(limitResult)
            .skip(skip)
            .getManyAndCount();
        return {
            data,
            total,
            page: Number(page) || 1,
            take: limitResult,
        };
    }
    async createCustomer(body: CustomerDto) {
        const { email } = body
        const userRepo = this.dataSource.getRepository(Customers)
        //check user exist
        const user = await userRepo.findOne({
            where: {
                email
            }
        })
        if (user) {
            await userRepo.update({ id: user.id }, body)
            return {
                userId: user.id
            }
        } else {
            const data = await userRepo.insert(body)
            return {
                userId: data.identifiers[0].id
            }
        }
    }

    async updateCustomer(id: number, body: CustomerDto) {
        const { email } = body
        const userRepo = this.dataSource.getRepository(Customers)
        const customer = await userRepo.findOne({ where: { id } })
        if (!customer) {
            throw new HttpException('Không tồn tại customer', HttpStatus.BAD_REQUEST);
        }

        //check email exist
        const checkEmail = await userRepo.findOne({
            where: {
                email,
                id: Not(id)
            }
        })
        if (checkEmail) {
            throw new HttpException('Email đã tồn tai', HttpStatus.BAD_REQUEST);
        }
        await userRepo.update({ id }, body)
        return {
            message: 'Update thành công'
        }
    }
}
