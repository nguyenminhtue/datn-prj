import { IsInt, IsNumber, IsOptional, IsString } from "class-validator";

export class RoomDto {
    @IsString()
    name: string;

    @IsInt()
    price: number;

    @IsString()
    @IsOptional()
    description: string;

    @IsInt()
    @IsOptional()
    type: number;
}