import { HttpException, HttpStatus, Injectable, Put } from '@nestjs/common';
import { Bookings } from 'entities/bookings.entity';
import { Comments } from 'entities/comments.entity';
import { Customers } from 'entities/customers.entity';
import { Rooms } from 'entities/rooms.entity';
import { DataSource, Not } from 'typeorm';
import { RoomDto } from './dto/create-room.dto';

@Injectable()
export class CommentService {
    constructor(private readonly dataSource: DataSource) { }
    async getListComment(roomId: number) {
        return this.dataSource.getRepository(Comments).find({
            where: { roomId },
            order: {
                id: 'DESC'
            }
        })
    }

    async createComment(body) {
        const { email } = body
        const data = await this.dataSource.getRepository(Bookings).createQueryBuilder('booking')
            .leftJoinAndMapOne('booking.customer', Customers, 'cus', 'cus.id = booking.customerId')
            .where({ isDeleted: 0 })
            .andWhere({ status: Not(1) })
            .getMany() as any
        if (data.length > 0) {
            const checkExist = data.find(item => item.customer.email === email)
            if (!checkExist) {
                throw new HttpException('Email chưa đặt phòng nên không thể bình luận', HttpStatus.BAD_REQUEST);

            }
        }
        return this.dataSource.getRepository(Comments).insert(body)
    }
}
