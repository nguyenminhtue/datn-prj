import { Body, Controller, Get, Post, UploadedFile, UseInterceptors, Query } from '@nestjs/common';
import { ParseIntPipe } from '@nestjs/common/pipes';
import { CommentService } from './comments.service';

@Controller('comments')
export class CommentsController {
    constructor(private readonly commentService: CommentService) {
    }
    @Get('/')
    async getListComment(@Query('roomId', ParseIntPipe) roomId: number) {
        return await this.commentService.getListComment(roomId);
    }

    @Post('/')
    async createComment(@Body() body) {
        return await this.commentService.createComment(body);
    }
}
