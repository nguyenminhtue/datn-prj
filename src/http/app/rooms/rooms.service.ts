import { HttpException, HttpStatus, Injectable, Put } from '@nestjs/common';
import { Rooms } from 'entities/rooms.entity';
import { DataSource } from 'typeorm';
import { RoomDto } from './dto/create-room.dto';

@Injectable()
export class RoomsService {
    constructor(private readonly dataSource: DataSource) { }

    async getListRooms(query) {
        const { search, take, page} = query
        const limitResult = Number(take) || 10;
        const skipResult = Number(page) || 1;
        const skip = (skipResult - 1) * limitResult;

        const rooms = this.dataSource.getRepository(Rooms).createQueryBuilder('room')
        if (search) {
            rooms.andWhere(
                `(room.name LIKE :search)`, { search: `%${search}%` }
            )
        }
        const [data, total] = await rooms
            .take(limitResult)
            .skip(skip)
            .getManyAndCount()
        return {
            data,
            total,
            page: Number(page) || 1,
            take: limitResult,
        }
    }

    async getDetailRoom(id: number) {
        return this.dataSource.getRepository(Rooms).findOne({ where: { id } })
    }

    async createRoom(body: RoomDto, file: any) {
        const { name, price, description } = body
        const checkName = await this.dataSource.getRepository(Rooms).findOne({ where: { name } })
        if (checkName) {
            throw new HttpException('Tên phòng đã tồn tại', HttpStatus.BAD_REQUEST);
        }

        const data = {
            name: name,
            price: Number(price)
        } as any
        if (description) data.description = description
        if (file) data.thumbnail = file.filename
        await this.dataSource.getRepository(Rooms).insert(data)

        return {
            message: "Tạo mới phòng thành công"
        }
    }

    async deleteRoom(id: number) {
        await this.dataSource.getRepository(Rooms).delete({ id })
        return {
            message: 'Xóa phòng thành công',
        };
    }

    async updateRoom(id: number, body) {
        await this.dataSource.getRepository(Rooms).update({ id }, body)
        return {
            message: 'Cập nhật thông tin phòng thành công',
        }
    }
}
