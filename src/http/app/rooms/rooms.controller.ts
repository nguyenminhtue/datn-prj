import { Body, Controller, Get, Post, UploadedFile, UseInterceptors, Query } from '@nestjs/common';
import { Delete, Param, Put } from '@nestjs/common/decorators';
import { ParseIntPipe } from '@nestjs/common/pipes';
import { FileInterceptor } from '@nestjs/platform-express';
import { multerOptions } from 'untils/functions';
import { RoomDto } from './dto/create-room.dto';
import { RoomsService } from './rooms.service';

@Controller('rooms')
export class RoomsController {
    constructor(private readonly roomsService: RoomsService) { }

    @Get('/')
    async getListRooms(@Query() query) {
        return await this.roomsService.getListRooms(query);
    }

    @Get('/detail')
    async getDetailRoom(@Query('id', ParseIntPipe) id: number) {
        return await this.roomsService.getDetailRoom(id);
    }


    @Put('/:id')
    async updateRoom(@Param('id', ParseIntPipe) id: number, @Body() body) {
        return await this.roomsService.updateRoom(id, body);
    }

    @Post('/')
    @UseInterceptors(FileInterceptor('file', multerOptions))
    async createRoom(@Body() body: RoomDto, @UploadedFile() file: Express.Multer.File) {
        return await this.roomsService.createRoom(body, file);
    }

    @Delete('/:id')
    async deleteRoom(@Param('id', ParseIntPipe) id: number) {
        return await this.roomsService.deleteRoom(id);
    }

}
