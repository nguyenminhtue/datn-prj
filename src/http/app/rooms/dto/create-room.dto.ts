import { IsInt, IsNumber, IsOptional, IsString } from "class-validator";

export class RoomDto {
    @IsString()
    name: string;

    @IsString()
    price: string;

    @IsString()
    @IsOptional()
    description: string;

    @IsInt()
    @IsOptional()
    type: number;
}