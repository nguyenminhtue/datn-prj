import { Module } from '@nestjs/common';
import { BillServiceController } from './bill-service.controller';
import { BillServiceService } from './bill-service.service';

@Module({
  controllers: [BillServiceController],
  providers: [BillServiceService]
})
export class BillServiceModule {}
