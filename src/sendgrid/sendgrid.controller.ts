import { Body, Controller, Headers, Post, UseGuards } from '@nestjs/common';
import { Public } from 'guards/public.guard';
import { checkTokenDto } from './dto/check-token.dto';
import { sendMailDto } from './dto/sendmail.dto';
import { SendgridService } from './sendgrid.service';

@Controller('mail')
export class SendgridController {
    constructor(private readonly sendgridService: SendgridService) { }

    @UseGuards(Public)
    @Post('/forgot-password')
    async sendMailForgotPass(@Headers('origin') origin: string, @Body() body: sendMailDto) {
        return await this.sendgridService.sendMailForgotPass(body, origin);
    }

    @UseGuards(Public)
    @Post('/check-expired-token')
    async checkExpiredToken(@Body() body: checkTokenDto) {
        return await this.sendgridService.checkExpiredToken(body);
    }
}
