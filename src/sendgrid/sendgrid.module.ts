import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { IAppConfig } from 'configs';
import { SendgridController } from './sendgrid.controller';
import { SendgridService } from './sendgrid.service';

@Module({
    imports: [
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => {
                const { appAccessTokenSecretKey, appAccessTokenExpiration } =
                    configService.get<IAppConfig>('commons').auth;
                return {
                    expiresIn: appAccessTokenExpiration,
                    secret: appAccessTokenSecretKey,
                };
            },
            inject: [ConfigService],
        }),
    ],
    controllers: [SendgridController],
    providers: [SendgridService],
    exports: [SendgridService],
})
export class SendgridModule {}
