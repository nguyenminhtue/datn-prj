import { IsEmail, IsString } from 'class-validator';

export class checkTokenDto {
    @IsString()
    token: string;
}
