import { IsEmail, IsString } from 'class-validator';

export class sendMailDto {
    @IsEmail()
    email: string;
}
