export function sendMail(
    email: string,
    token: string,
    subject: string,
    text: string,
    type: string
) {
    const emailConfig = process.env.SEND_GRID_EMAIL;
    const sendGridLink = process.env.SEND_GRID_LINK;
    return {
        to: email,
        subject,
        from: emailConfig,
        html: `<p>${text}: ${sendGridLink + type}?token=${token}</p>`,
    };
}
