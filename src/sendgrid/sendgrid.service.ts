import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as SendGrid from '@sendgrid/mail';
import { IAppConfig } from 'configs';
import { UserAccounts } from 'entities/user-accounts.entity';
import { DataSource } from 'typeorm';
import { checkTokenDto } from './dto/check-token.dto';
import { sendMailDto } from './dto/sendmail.dto';
import * as moment from 'moment';
import {
    ErrorMessage,
    StatusDeleteAccount,
    SuccessMessage,
    TemplateSendGrid,
    TypeSendMail,
} from 'enums';
import { verify } from 'jsonwebtoken';
import { sendMail } from './templates/template-send-mail';
import { createToken } from 'http/app/auth/utils/create-token-public';

@Injectable()
export class SendgridService {
    constructor(
        private readonly configService: ConfigService,
        private readonly dataSource: DataSource
    ) {
        SendGrid.setApiKey(this.configService.get<string>('SEND_GRID_KEY'));
    }

    async sendMailForgotPass(body: sendMailDto, origin: string) {
        const user = await this.dataSource
            .getRepository(UserAccounts)
            .createQueryBuilder()
            .where({ email: body.email })
            .getOne();

        if (!user) {
            throw new HttpException(ErrorMessage.EmailNotExist, HttpStatus.BAD_REQUEST);
        }
        const token = await createToken(body);
        this.sendMail(token, body.email, origin, TypeSendMail.ResetPassword, TemplateSendGrid.ResetPassword);
    }

    async sendMail(token: string, email: string, origin: string, type: string, templateId: string, shareModel = false, member = null) {
        const userRepo = this.dataSource.getRepository(UserAccounts).createQueryBuilder('user');
        if (!shareModel) {
            const emailItem = await this.dataSource
                .getRepository(UserAccounts)
                .createQueryBuilder()
                .where({ email })
                .getOne();
            if (!emailItem) {
                throw new HttpException(ErrorMessage.EmailNotExist, HttpStatus.BAD_REQUEST);
            }
        }
        const originSendmail = origin ? origin : 'http://localhost:3000'
        const dataSendMail = {
            from: process.env.SEND_GRID_EMAIL,
            personalizations: [
                {
                    "to": [
                        {
                            "email": email
                        }
                    ],
                    dynamicTemplateData: {
                        "url": `${originSendmail}/${type}?token=${token}`,
                        "username": member?.name,
                        "to": email,
                        "from": member?.email
                    }
                },
            ],
            templateId
        }

        SendGrid.send(dataSendMail)
            .then(async () => {
                await userRepo
                    .update()
                    .set({
                        token,
                        timeUpdateToken: new Date(),
                    })
                    .where({ email })
                    .execute();
            })
            .catch(() => {
                throw new HttpException(ErrorMessage.BadRequest, HttpStatus.BAD_REQUEST);
            });
    }

    async checkExpiredToken(body: checkTokenDto) {
        const { token } = body;
        const { appAccessTokenSecretKey } = this.configService.get<IAppConfig>('commons').auth;
        try {
            const decode = verify(token, appAccessTokenSecretKey) as any;
            const user = await this.dataSource
                .getRepository(UserAccounts)
                .createQueryBuilder()
                .where({ email: decode?.email })
                .andWhere({ isDeleted: StatusDeleteAccount.NoDelete })
                .getOne();

            if (user.token !== token || moment().diff(moment(user.timeUpdateToken), 'minute') > 30)
                throw new HttpException(ErrorMessage.BadRequest, HttpStatus.BAD_REQUEST);
            return {
                message: SuccessMessage.TokenValid,
            };
        } catch (error) {
            throw new HttpException(ErrorMessage.TokenExpired, HttpStatus.BAD_REQUEST);
        }
    }
}
