import { registerAs } from '@nestjs/config';

export interface IAppConfig {
    port: number;
    environment: string;
    database: {
        type: 'mysql';
        host: string;
        port: number;
        username: string;
        password: string;
        database: string;
        synchronize: boolean;
        timezone: string;
        logging: boolean;
        entities: string[];
        migrations: string[];
    };
    throttler: {
        ttl: number;
        limit: number;
    };
    auth: {
        appAccessTokenSecretKey: string;
        appRefreshTokenSecretKey: string;
        appAccessTokenExpiration: string;
        appRefreshTokenExpiration: string;
        saltOrRounds: number;
        verifyCodeExpiration: number;
        twitterConsumerKey: string;
        twitterConsumerSecret: string;
        nestSession: string;
        facebookAppId: string;
        facebookAppSecret: string;
    };
}

export default registerAs('commons', () => ({
    port: Number(process.env.PORT),
    environment: process.env.ENVIRONMENT,
    database: {
        type: 'mysql',
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
        synchronize: true,
        timezone: 'Z',
        logging: false,
        entities: [__dirname + '/../entities/*.entity{.js, .ts}'],
        migrations: [__dirname + '/../database/migrations/*'],
    },
    throttler: {
        ttl: Number(process.env.THROTTLE_TTL),
        limit: Number(process.env.THROTTLE_LIMIT),
    },
    auth: {
        appAccessTokenSecretKey: process.env.APP_ACCESS_TOKEN_SECRET_KEY,
        appRefreshTokenSecretKey: process.env.APP_REFRESH_TOKEN_SECRET_KEY,
        appAccessTokenExpiration: process.env.APP_ACCESS_TOKEN_EXPIRATION,
        appRefreshTokenExpiration: process.env.APP_REFRESH_TOKEN_EXPIRATION,
        saltOrRounds: Number(process.env.SALT_OR_ROUNDS),
        verifyCodeExpiration: Number(process.env.VERIFY_CODE_EXPIRATION),
        twitterConsumerKey: process.env.TWITTER_CONSUMER_KEY,
        twitterConsumerSecret: process.env.TWITTER_CONSUMER_SECRET,
        nestSession: process.env.NEST_SESSION,
        facebookAppId: process.env.FACEBOOK_APP_ID,
        facebookAppSecret: process.env.FACEBOOK_APP_SECRET,
    },
}));
