import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { IAppConfig } from 'configs';
import { Request } from 'express';
import { Strategy } from 'passport-facebook';

@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
    constructor(private readonly configService: ConfigService) {
        super({
            clientID: configService.get<IAppConfig>('commons').auth.facebookAppId,
            clientSecret: configService.get<IAppConfig>('commons').auth.facebookAppSecret,
            callbackURL: 'https://api.hac-pb.local/auth/login-facebook/callback',
            passReqToCallback: true,
        });
    }

    async validate(req: Request, accessToken: string, refreshToken: string, profile: any) {
        return profile;
    }
}
