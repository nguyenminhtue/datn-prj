import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { IAppConfig } from 'configs';
import { Request } from 'express';
import { Strategy } from 'passport-twitter';

@Injectable()
export class TwitterStrategy extends PassportStrategy(Strategy, 'twitter') {
    constructor(private readonly configService: ConfigService) {
        super({
            consumerKey: configService.get<IAppConfig>('commons').auth.twitterConsumerKey,
            consumerSecret: configService.get<IAppConfig>('commons').auth.twitterConsumerSecret,
            callbackURL: 'http://127.0.0.1:5000/auth/login-twitter/callback',
            passReqToCallback: true,
            includeEmail: true,
            skipExtendedUserProfile: false,
        });
    }

    async validate(req: Request, token: string, tokenSecret: string, profile: any) {
        return profile;
    }
}
