import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { IAppConfig } from 'configs';
import { Request } from 'express';
import { verify } from 'jsonwebtoken';
import { MemberService } from 'http/app/member/member.service';
import { CommonStatus, ErrorMessage } from 'enums';

@Injectable()
export class PublicStrategy extends PassportStrategy(Strategy, 'public') {
    constructor(
        private readonly configService: ConfigService,
        private readonly memberService: MemberService
    ) {
        super({
            jwtFromRequest: (req: Request) => {
                let token = null;

                if (req.header('authorization')) {
                    token = req.header('authorization').split(' ')[1];
                }

                return token;
            },
            ignoreExpiration: false,
            secretOrKey: configService.get<IAppConfig>('commons').auth.appAccessTokenSecretKey,
            passReqToCallback: true,
        });
    }

    async validate(req: Request, payload: any) {
        const member = await this.memberService.getMemberInfoById(payload?.id);
        if (member?.isDeleted === CommonStatus.InActive) {
            throw new HttpException(ErrorMessage.MemberBlocked, HttpStatus.UNAUTHORIZED);
        }

        req.memberId = member?.id;
        return payload;
    }
}
