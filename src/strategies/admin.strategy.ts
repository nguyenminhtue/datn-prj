import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { IAppConfig } from 'configs';
import { CommonStatus, ErrorMessage, Role } from 'enums';
import { Request } from 'express';
import { MemberService } from 'http/app/member/member.service';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class AdminStrategy extends PassportStrategy(Strategy, 'admin') {
    constructor(
        private readonly configService: ConfigService,
        private readonly memberService: MemberService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get<IAppConfig>('commons').auth.appAccessTokenSecretKey,
            passReqToCallback: true,
        });
    }

    async validate(req: Request, payload: any) {
        const member = await this.memberService.getMemberInfoById(payload?.id);
        if (member?.isDeleted === CommonStatus.InActive) {
            throw new HttpException(ErrorMessage.MemberBlocked, HttpStatus.UNAUTHORIZED);
        }
        if (payload?.roleId !== Role.Admin) {
            throw new HttpException(ErrorMessage.Unauthorized, HttpStatus.UNAUTHORIZED);
        }
        req.memberId = member?.id;
        return payload;
    }
}
