### STAGE 1: Install Dependencies ###
FROM node:16.15.1-alpine AS dev
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install --silent

### STAGE 2: Build ###
FROM node:16.15.1-alpine AS build
WORKDIR /app
COPY --from=dev /app/node_modules /app/node_modules
COPY . .
RUN npm run build && npm install --production --silent

### STAGE 3: Production ###
FROM node:16.15.1-alpine
LABEL author='Pham Tien Duc <duc.pt172483@gmail.com>'
WORKDIR /app
COPY --from=build /app/dist /app/dist
COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/package.json /app/package.json
COPY --from=build /app/.env /app/.env

EXPOSE 5000
CMD ["npm", "run", "start:prod"]