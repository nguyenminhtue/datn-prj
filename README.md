# DATN

Evironment variables

```sh
$ cp .env.example .env
```

Start docker-compose development server

```sh
$ npm run docker-compose:dev-up
```

Down docker-compose development server

```sh
$ npm run docker-compose:dev-down
```

Database management

```sh
http://127.0.0.1:8080/

Server: mysql
Username: root
Password: root
Database: datn
```
